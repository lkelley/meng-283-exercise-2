class Timer
{
private:
    unsigned long startTime;
    unsigned long duration;

public:
    Timer();
    Timer(unsigned long);

    bool getExpired();
    void setDuration(unsigned long);
    void start();
    void start(unsigned long);
};
