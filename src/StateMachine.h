/**
 * Simple Finite State Machine class that tracks states of the simple game
 */

class StateMachine
{
private:
    short currentState;

public:
    const static short WATING = 1;
    const static short FLASHING = 2;
    const static short PRIMED = 3;
    const static short COMPLETE = 4;

    StateMachine();
    short getState();
    void setState(short);
};
