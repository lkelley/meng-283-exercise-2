/**
 * Button class to manage the state of a pushbutton, with integrated debouncing
 */

class Button
{
    private:
        short _pin; // Digital pin the button is bound to
        unsigned int _debounceTime; // Time to debounce the button
        unsigned int _lastDebounce; // Time of the last debounce check
        int _buttonState; // State as read
        int _lastButtonState; // State last time the button was read
    
    public:
        Button();
        void setPin(int);
        int getState();
};
