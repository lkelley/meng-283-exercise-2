#include <Arduino.h>
#include "Timer.h"


Timer::Timer() {}


Timer::Timer(unsigned long duration) {
    this->duration = duration;
}


void Timer::start() {
    this->startTime = millis();
}


void Timer::start(unsigned long duration) {
    this->duration = duration;
    this->startTime = millis();
}


void Timer::setDuration(unsigned long duration) {
    this->duration = duration;
}


bool Timer::getExpired() {
    return millis() >= this->startTime + this->duration;
}
