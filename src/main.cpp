/**
 * A simple game for 2 players. Players will stage in front
 * of two buttons, one for each player.
 * 
 * When both players have pressed and held their buttons,
 * the red LED will flash 7 times. After the flashes, the
 * red LED will stay on.
 * 
 * At this point, each player will attempt to be the first
 * to release their button. The first to do so wins, as
 * indicated by the green light closest to the player's
 * button.
 * 
 * Written for Arduino Uno
 * By Terence Kelley
 */

#include <Arduino.h>
#include "StateMachine.h"
#include "Button.h"
#include "Timer.h"

// LED constants
const short RED_LED = 1,
PLAYER_1_GREEN_LED = 2,
PLAYER_2_GREEN_LED = 3,
PLAYER_1_BUTTON = 4,
PLAYER_2_BUTTON = 5;

// Half period of the red LED flash
const unsigned long FLASH_DURATION = 250;

// Forward declaration of reset()
void reset();

// sm tracks the operating states of the game
StateMachine sm;

// Button objects
Button buttonPlayer1;
Button buttonPlayer2;

// ID of victorious player
short victoriousPlayer;

// Timer object and related vars for LED flashing
Timer timer;
short flashCount;
const short FLASHES = 7;


/**
 * Initial setup for I/O and counter vars
 */
void setup() {
    // Set LED outputs and BUTTON inputs
    pinMode(13, OUTPUT);
    pinMode(RED_LED, OUTPUT);
    pinMode(PLAYER_1_GREEN_LED, OUTPUT);
    pinMode(PLAYER_2_GREEN_LED, OUTPUT);
    pinMode(PLAYER_1_BUTTON, INPUT);
    pinMode(PLAYER_2_BUTTON, INPUT);

    // Set buttons
    buttonPlayer1.setPin(PLAYER_1_BUTTON);
    buttonPlayer2.setPin(PLAYER_2_BUTTON);

    // Call game reset
    reset();

    // Flash pin 13 LED 1 time at 1 Hz
    digitalWrite(13, HIGH);
    delay(500);
    digitalWrite(13, LOW);
    delay(500);
}


/**
 * Resets LED and counter states
 */
void reset() {
    digitalWrite(RED_LED, LOW);
    digitalWrite(PLAYER_1_GREEN_LED, LOW);
    digitalWrite(PLAYER_2_GREEN_LED, LOW);
    flashCount = 0;
}


/**
 * Main Loop
 */
void loop() {
    // FSM
    switch (sm.getState()) {
        /*
        Start of game, waiting for player interaction
        */
        case StateMachine::WATING: {
            // When both players are holding their buttons, switch to FLASHING state
            if (buttonPlayer1.getState() && buttonPlayer2.getState()) {
                sm.setState(StateMachine::FLASHING);
            }
            break;
        }

        /*
        Flashing state
        */
        case StateMachine::FLASHING: {
            /*
            flashCount is incremented each time the LED turns on
            or off. Therefore it must reach a value of 2 * FLASHES.
            */
            if (flashCount >= FLASHES * 2) {
                sm.setState(StateMachine::PRIMED);
            }

            // Player 1 released button early
            else if (!buttonPlayer1.getState()) {
                victoriousPlayer = 2;
                sm.setState(StateMachine::COMPLETE);
            }

            // Player 2 released button early
            else if (!buttonPlayer2.getState()) {
                victoriousPlayer = 1;
                sm.setState(StateMachine::COMPLETE);
            }

            // Check flash timer
            else if (timer.getExpired()) {
                // Toggle LED
                digitalWrite(RED_LED, !digitalRead(RED_LED));

                // Restart flash timer
                timer.start(FLASH_DURATION);

                // Count flash
                flashCount++;
            }

            break;
        }

        /*
        Primed state.

        In this state, players compete to release their button
        first.
        */
        case StateMachine::PRIMED: {
            // Player 1 victory
            if (!buttonPlayer1.getState()) {
                victoriousPlayer = 1;
                sm.setState(StateMachine::COMPLETE);
            }

            // Player 2 victory
            else if (!buttonPlayer2.getState()) {
                victoriousPlayer = 2;
                sm.setState(StateMachine::COMPLETE);
            }

            break;
        }

        /*
        Complete state.

        A player has been victorious.
        */
        case StateMachine::COMPLETE: {
            digitalWrite(RED_LED, LOW);

            switch (victoriousPlayer) {
                case 1: {
                    digitalWrite(PLAYER_1_GREEN_LED, HIGH);
                    break;
                }

                case 2: {
                    digitalWrite(PLAYER_2_GREEN_LED, HIGH);
                    break;
                }

                default: {
                    digitalWrite(RED_LED, HIGH);
                }
            }

            // Time to reset
            delay(5000);

            reset();
            sm.setState(StateMachine::WATING);

            break;
        }

        // Default catch block to return to WAITING
        default: {
            sm.setState(StateMachine::WATING);
        }
    }
}