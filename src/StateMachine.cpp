#include "StateMachine.h"

StateMachine::StateMachine() {
    this->currentState = StateMachine::WATING;
}

short StateMachine::getState() {
    return this->currentState;
}

void StateMachine::setState(short newState) {
    this->currentState = newState;
}
